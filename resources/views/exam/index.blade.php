@extends('layouts.app')

@section('content')
<h1>Lista de Examenes</h1>

<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>title</th>
        <th>date</th>
        <th>user</th>
        <th>module</th>
        <th>Acciones</th>
    </tr>
    @foreach($exams as $exam)
    <tr>
        <td>{{$exam->id}}</td>
        <td>{{$exam->title}}</td>
        <td>{{$exam->date}}</td>
        <td>{{$exam->user_id}}</td>
        <td>{{$exam->module->name}}</td>
        <td>{{$exam->user->name}}</td>
        <td>Botones</td>
    </tr>
    @endforeach
</table>
{{ $exams->links() }}
@endsection
