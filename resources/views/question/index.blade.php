@extends('layouts.app')

@section('content')
<h1>Lista de Preguntas</h1>

<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>text</th>
        <th>a</th>
        <th>b</th>
        <th>c</th>
        <th>d</th>
        <th>answer</th>
        <th>module_id</th>
    </tr>
    @foreach($questions as $question)
    <tr>
        <td>{{$question->id}}</td>
        <td>{{$question->text}}</td>
        <td>{{$question->a}}</td>
        <td>{{$question->b}}</td>
        <td>{{$question->c}}</td>
        <td>{{$question->d}}</td>
        <td>{{$question->module_id}}</td>
        <td>Botones</td>
    </tr>
    @endforeach
</table>
@endsection
