
@extends('layouts.app')
@section('content')

<div class="container">
    <h1>Create Question</h1>
    <form action="/questions"  method="post">
        {{ csrf_field() }}
        <label>Texto:</label>
            <input type="text" name="text">
            <br>
          <label>A:</label>
            <input type="text" name="a">
          <label>B:</label>
            <input type="text" name="b">
          <label>C:</label>
            <input type="text" name="c">
          <label>D:</label>
            <input type="text" name="d">
          <label>Respuesta:</label>
            <input type="text" name="answer">

          <label>Modulo:
            <select name="module">
                @foreach($modules as $module)
                <option value="{{$module->id}}">{{$module->name}}</option>
               @endforeach
            </select>
        </label>
        <input type="submit" name="save" value="Save">
    </form>
</div>
@endsection
